package de.maggicraft.mlog;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * created on 2020.10.16 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
@SuppressWarnings("CallToPrintStackTrace")
public abstract class AbstractLog implements ILog {

   protected static final String LINE_SEP = System.lineSeparator();

   @NotNull
   protected DateTimeFormatter mFormatter;
   @Nullable
   protected String mHeader;
   /**
    * Prints the version of the JRE executing this programm if this variable is positive (not 0).
    * Each time a something is written to the log this variable gets decremented.
    *
    * @see MLogImpl#log(String, EDisplayHeader)
    */
   @NotNull
   protected final AtomicInteger mDisplayJavaVersion = new AtomicInteger(1);
   @NotNull
   private EDisplayMessage mDisplayMessage;

   protected AbstractLog(@NotNull EDisplayMessage pDisplayMessage) {
      mFormatter = DateTimeFormatter.ofPattern("[dd.MM.yyyy-kk:mm:ss:SSS]: ", Locale.ENGLISH);
      mDisplayMessage = pDisplayMessage;
   }

   @Override
   public String toString() {
      return "AbstractLog{ format=" + mFormatter + ", mHeader='" + mHeader + '\'' +
             ", displayJavaVersion=" + mDisplayJavaVersion + ", displayMessages=" +
             mDisplayMessage + " }";
   }

   @Override
   public void log(@NotNull Throwable pStacktrace) {
      log(pStacktrace, EDisplayHeader.DISPLAY_HEADER);
   }

   @Override
   public void log(@NotNull String pText) {
      log(pText, EDisplayHeader.DISPLAY_HEADER);
   }

   @Override
   public void log(@NotNull String pText, @NotNull Throwable pStacktrace) {
      log(pText, pStacktrace, EDisplayHeader.DISPLAY_HEADER);
   }

   @Override
   public void logNoAnalytics(@NotNull String pText) {
      log(pText);
   }

   @Override
   public void logNoAnalytics(@NotNull Throwable pStacktrace) {
      log(pStacktrace);
   }

   @Override
   public void logNoAnalytics(@NotNull String pText, @NotNull Throwable pStacktrace) {
      log(pText, pStacktrace);
   }

   public void log(@NotNull Throwable pStacktrace, @NotNull EDisplayHeader pDisplayHeader) {
      try (Writer result = new StringWriter()) {
         pStacktrace.printStackTrace(new PrintWriter(result));
         log(result.toString(), pDisplayHeader);
      } catch (IOException pE) {
         if (displayMessage()) {
            pE.printStackTrace();
         }
      }
   }

   public void log(@NotNull String pText, @NotNull Throwable pStacktrace,
         @NotNull EDisplayHeader pDisplayHeader) {
      try (Writer result = new StringWriter()) {
         pStacktrace.printStackTrace(new PrintWriter(result));
         log(pText + '\t' + result, pDisplayHeader);
      } catch (IOException pE) {
         if (displayMessage()) {
            pE.printStackTrace();
         }
      }
   }

   public abstract void log(@NotNull String pText, @NotNull EDisplayHeader pDisplayHeader);

   @NotNull
   @Contract(pure = true)
   protected String processHeader(@NotNull String pText, @NotNull EDisplayHeader pDisplayHeader) {
      if (pDisplayHeader == EDisplayHeader.DISPLAY_HEADER) {
         String jreVersion = (mDisplayJavaVersion.get() > 0) ?
                             (System.getProperty("java.version") + ' ') : "";
         String timeStamp = getTimeStamp();
         return mHeader + jreVersion + timeStamp + pText + LINE_SEP;
      } else {
         return pText + LINE_SEP;
      }
   }

   public void setDisplayMessage(@NotNull EDisplayMessage pDisplayMessage) {
      mDisplayMessage = pDisplayMessage;
   }

   /**
    * @return an timestamp that is used as an header.
    */
   @NotNull
   @Contract(pure = true)
   protected String getTimeStamp() {
      LocalDateTime timePoint = LocalDateTime.now();
      return mFormatter.format(timePoint) + LINE_SEP;
   }

   protected boolean displayMessage() {
      return mDisplayMessage == EDisplayMessage.DISPLAY_MESSAGE;
   }

   /**
    * Sets the format of the timestamp used in {@link MLogImpl#getTimeStamp()}
    */
   public void setFormatter(@NotNull DateTimeFormatter pFormatter) {
      mFormatter = pFormatter;
   }

   public void setHeader(@Nullable String pHeader) {
      mHeader = pHeader;
   }

   public void setDisplayJavaVersion(int pDisplayJavaVersion) {
      mDisplayJavaVersion.set(pDisplayJavaVersion);
   }

   public enum EDisplayMessage {
      DISPLAY_MESSAGE, SKIP_MESSAGE
   }

   public enum EDisplayHeader {
      DISPLAY_HEADER, SKIP_HEADER
   }
}
