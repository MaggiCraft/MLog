package de.maggicraft.mlog;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 * Writes stacktraces and text into a file, optional with a timestamp.
 * <p>
 * Created by Marc Schmidt on 23.March.2017<br> mail: MaggiCraftInfo@gmail.com<br> gitlab:
 * https://gitlab.com/MaggiCraft
 */
@SuppressWarnings("CallToPrintStackTrace")
public class MLogImpl extends AbstractLog {

   @NotNull
   private final Object mLock = new Object();
   @NotNull
   private final Path mOutput;

   /**
    * Sets the given file as the file into which all text and stacktraces are written. If the
    * argument {@code pDisplayMessages} is {@code true}, then all written text will be displayed.
    *
    * @param pOutput         file into which all text and stacktraces are written
    * @param pDisplayMessage if {@code pDisplayMessages} is {@code true}, then all written text will
    *                        be displayed.
    */
   public MLogImpl(@NotNull Path pOutput, @NotNull EDisplayMessage pDisplayMessage) {
      super(pDisplayMessage);
      mOutput = pOutput;

      try {
         Files.createDirectories(mOutput.getParent());
      } catch (IOException pE) {
         if (pDisplayMessage == EDisplayMessage.DISPLAY_MESSAGE) {
            pE.printStackTrace();
         }
      }
   }

   @Override
   public String toString() {
      return "MLogImpl{ output=" + mOutput + ", format=" + mFormatter + ", header='" + mHeader +
             '\'' + ", displayMessage=" + displayMessage() + ", displayJavaVersion=" +
             mDisplayJavaVersion + " }";
   }

   /**
    * Writes the given text into {@link MLogImpl#mOutput}. If the argument {@code pHeader} is true,
    * then the timestamp will be written in front of {@code pText}.
    *
    * @param pText          text to be written
    * @param pDisplayHeader if {@link EDisplayHeader#DISPLAY_HEADER}, then a timestamp will be
    *                       written.
    * @see MLogImpl#getTimeStamp()
    */
   @SuppressWarnings("UseOfSystemOutOrSystemErr")
   @Override
   public void log(@NotNull String pText, @NotNull EDisplayHeader pDisplayHeader) {
      synchronized (mLock) {
         pText = processHeader(pText, pDisplayHeader);
         if (displayMessage()) {
            System.err.print(pText);
         }

         try (BufferedWriter writer = Files.newBufferedWriter(mOutput, StandardOpenOption.APPEND)) {
            writer.write(pText);
            writer.flush();
         } catch (IOException pE) {
            if (displayMessage()) {
               pE.printStackTrace();
            }
         }
      }
      mDisplayJavaVersion.decrementAndGet();
   }

   @NotNull
   public Path getOutput() {
      return mOutput;
   }

}
