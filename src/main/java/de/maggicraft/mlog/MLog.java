package de.maggicraft.mlog;

import de.maggicraft.mlog.AbstractLog.EDisplayMessage;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * created on 2019.02.12 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
public final class MLog {

   @NotNull
   private static ILog sLog = new MVoidLog(EDisplayMessage.DISPLAY_MESSAGE);

   @Contract(pure = true)
   private MLog() { }

   public static void init(@NotNull ILog pLog) {
      sLog = pLog;
   }

   public static void log(@NotNull Throwable pStacktrace) {
      sLog.log(pStacktrace);
   }

   public static void log(@NotNull String pText) {
      sLog.log(pText);
   }

   public static void log(@NotNull String pText, @NotNull Throwable pStacktrace) {
      sLog.log(pText, pStacktrace);
   }

   public static void logNoAnalytics(@NotNull String pText) {
      sLog.logNoAnalytics(pText);
   }

   public static void logNoAnalytics(@NotNull Throwable pStacktrace) {
      sLog.logNoAnalytics(pStacktrace);
   }

   public static void logNoAnalytics(@NotNull String pText, @NotNull Throwable pStacktrace) {
      sLog.logNoAnalytics(pText, pStacktrace);
   }

   @Contract(pure = true)
   public static ILog getLog() {
      return sLog;
   }
}
