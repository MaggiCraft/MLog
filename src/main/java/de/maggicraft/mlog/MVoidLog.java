package de.maggicraft.mlog;

import org.jetbrains.annotations.NotNull;

/**
 * created on 2019.04.20 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
public class MVoidLog extends AbstractLog {

   @NotNull
   private final Object mLock = new Object();

   public MVoidLog(@NotNull EDisplayMessage pDisplayMessage) {
      super(pDisplayMessage);
   }

   @Override
   public String toString() {
      return "MVoidLog{ lock=" + mLock + ", formatter=" + mFormatter + ", header='" + mHeader +
             '\'' + ", displayJavaVersion=" + mDisplayJavaVersion + " }";
   }

   @SuppressWarnings("UseOfSystemOutOrSystemErr")
   @Override
   public void log(@NotNull String pText, @NotNull EDisplayHeader pDisplayHeader) {
      synchronized (mLock) {
         pText = processHeader(pText, pDisplayHeader);
         if (displayMessage()) {
            System.err.print(pText);
         }
      }
      mDisplayJavaVersion.decrementAndGet();
   }
}
