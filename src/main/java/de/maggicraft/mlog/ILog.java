package de.maggicraft.mlog;

import org.jetbrains.annotations.NotNull;

/**
 * created on 2019.02.12 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
public interface ILog {

   void log(@NotNull Throwable pStacktrace);

   void log(@NotNull String pText);

   void log(@NotNull String pText, @NotNull Throwable pStacktrace);

   void logNoAnalytics(@NotNull String pText);

   void logNoAnalytics(@NotNull Throwable pStacktrace);

   void logNoAnalytics(@NotNull String pText, @NotNull Throwable pStacktrace);

}
